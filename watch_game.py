#coding=utf-8

#import copy
import glob
import os
import time
import types
from collections import deque
from multiprocess import Pool

import numpy as np
import torch
from scipy import linalg
from math import sqrt, exp, floor, log

import algo
from arguments import get_args
from envs import make_vec_envs
from models import create_policy
from rollout_storage import RolloutStorage
from replay_storage import ReplayStorage
from visualize import visdom_plot

from models.model_pomm import PommNet

from distributions import Categorical, DiagGaussian

from models.policy import Policy

args = get_args()
args.num_steps = 10

assert args.algo in ['a2c', 'a2c-sil', 'ppo', 'ppo-sil', 'acktr']
if args.recurrent_policy:
    assert args.algo in ['a2c', 'ppo'], \
        'Recurrent policy is not implemented for ACKTR or SIL'

update_factor = args.num_steps * args.num_processes
num_updates = int(args.num_frames) // update_factor
lr_update_schedule = None if args.lr_schedule is None else args.lr_schedule // update_factor

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)
np.random.seed(args.seed)

try:
    os.makedirs(args.log_dir)
except OSError:
    files = glob.glob(os.path.join(args.log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)

eval_log_dir = args.log_dir + "_eval"
try:
    os.makedirs(eval_log_dir)
except OSError:
    files = glob.glob(os.path.join(eval_log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)


# From https://github.com/ctallec/world-models/blob/master/utils/misc.py
def flatten_parameters(params):
    """ Flattening parameters.
    :args params: generator of parameters (as returned by module.parameters())
    :returns: flattened parameters (i.e. one tensor of dimension 1 with all
        parameters concatenated)
    """
    #return torch.cat([p.detach().view(-1) for p in params], dim=0).cpu().numpy()
    return torch.cat([p.detach().view(-1) for p in params], dim=0)


def load_parameters(params, controller):
    """ Load flattened parameters into controller.
    :args params: parameters as a single 1D np array
    :args controller: module in which params is loaded
    """
    proto = next(controller.parameters())
    params = unflatten_parameters(
        params, controller.parameters(), proto.device)

    for p, p_0 in zip(controller.parameters(), params):
        p.data.copy_(p_0)


def unflatten_parameters(params, example, device):
    """ Unflatten parameters.
    :args params: parameters as a single 1D np array
    :args example: generator of parameters (as returned by module.parameters()),
        used to reshape params
    :args device: where to store unflattened parameters
    :returns: unflattened parameters
    """
    #params = torch.Tensor(params).to(device)
    idx = 0
    unflattened = []
    for e_p in example:
        unflattened += [params[idx:idx + e_p.numel()].view(e_p.size())]
        idx += e_p.numel()
    return unflattened



def main():
    #torch.set_num_threads(1)
    # torch.device("cuda:0" if args.cuda else "cpu")
    device = torch.device("cpu")
    print(device)

    if args.vis:
        from visdom import Visdom
        viz = Visdom(port=args.port)
        win = None

    # Consider calling something without _vec_..

    train_envs = make_vec_envs(
        args.env_name, args.seed, 1, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)

    obs_space = train_envs.observation_space
    action_space = train_envs.action_space
    obs_shape = obs_space.shape

    # Create network
    start_time = time.time()
    convtype = None
    final_net = PommNet(obs_shape=obs_shape, cnn_config=convtype)

    final_param_np = np.load('runs/result_hej_final.npy') # load parameter set. CHange accorsing to run. 
    final_param = torch.from_numpy(final_param_np)
    load_parameters(final_param, final_net)


    # Play single game.
    actor_evo = Policy(final_net, action_space=action_space)

    train_envs = make_vec_envs(
        args.env_name, args.seed, 1, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)  # Create single training environment instead of num process

    obs = train_envs.reset()

    
    done = False
    count = 0
    num_plays = 100
    while not done and count < num_plays:     
        
        with torch.no_grad():
            value, action, action_log_prob = actor_evo.act2(obs)

        obs, reward, done, infos = train_envs.step(action)
        
        count += 1

    print(count)
    print(reward)



if __name__ == "__main__":
    main()
