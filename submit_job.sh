#!/bin/sh
# submit_job.sh
# Submits neural network to the cluster for testing.
# virtenv folder is NOT copied. Must be in the same folder as this script.

module load python3

# Kill test if already running
# Actually don't as they're called the same... If jobname is different for each run then this is fine
#bkill -J pommerman_evolution

# Create all needed folders
#mkdir -p $DPATH $FPATH $LPATH
RUN_FOLDER=RUN24
echo "Creating directories"
mkdir -p $RUN_FOLDER
mkdir -p $RUN_FOLDER/tmp_logs
mkdir -p $RUN_FOLDER/runs

# Copy all to RUN
echo "Copying"
cp -rft $RUN_FOLDER algo envs helpers models arguments.py cpujobscript.sh distributions.py enjoy.py main_ES.py \
 replay_storage.py rollout_storage.py utils.py visualize.py

# Move into RUN and submit job
echo "Moving into directory and running job"
cd $RUN_FOLDER
bsub < cpujobscript.sh
cd ../