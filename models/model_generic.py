import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class Flatten(nn.Module):
    def forward(self, x):
        return x.view(x.size(0), -1)


class NNBase(nn.Module):

    def __init__(self, recurrent, recurrent_input_size, hidden_size):
        super(NNBase, self).__init__()

        self._hidden_size = hidden_size
        self._recurrent = recurrent

        if recurrent:
            self.gru = nn.GRUCell(recurrent_input_size, hidden_size)
            nn.init.orthogonal_(self.gru.weight_ih.data)
            nn.init.orthogonal_(self.gru.weight_hh.data)
            self.gru.bias_ih.data.fill_(0)
            self.gru.bias_hh.data.fill_(0)

    @property
    def is_recurrent(self):
        return self._recurrent

    @property
    def recurrent_hidden_state_size(self):
        if self._recurrent:
            return self._hidden_size
        return 1

    @property
    def output_size(self):
        return self._hidden_size

    def _forward_gru(self, x, hxs, masks):
        if x.size(0) == hxs.size(0):
            x = hxs = self.gru(x, hxs * masks)
        else:
            # x is a (T, N, -1) tensor that has been flatten to (T * N, -1)
            N = hxs.size(0)
            T = int(x.size(0) / N)

            # unflatten
            x = x.view(T, N, x.size(1))

            # Same deal with masks
            masks = masks.view(T, N, 1)

            outputs = []
            for i in range(T):
                hx = hxs = self.gru(x[i], hxs * masks[i])
                outputs.append(hx)

            # assert len(outputs) == T
            # x is a (T, N, -1) tensor
            x = torch.stack(outputs, dim=0)
            # flatten
            x = x.view(T * N, -1)

        return x, hxs


class CNNBase(NNBase):
    def __init__(self, num_inputs, recurrent=False, hidden_size=512):
        super(CNNBase, self).__init__(recurrent, hidden_size, hidden_size)

        self.main = nn.Sequential(
            nn.Conv2d(num_inputs, 32, 8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, 4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 32, 3, stride=1),
            nn.ReLU(),
            Flatten(),
            nn.Linear(32 * 7 * 7, hidden_size),
            nn.ReLU()
        )

        self.critic_linear = nn.Linear(hidden_size, 1)

    def forward(self, inputs, rnn_hxs, masks):
        x = self.main(inputs / 255.0)

        if self.is_recurrent:
            x, rnn_hxs = self._forward_gru(x, rnn_hxs, masks)

        return self.critic_linear(x), x, rnn_hxs


### DEFINED BY US ###
class lorenzNet(NNBase):
   def __init__(self, obs_shape,  recurrent=False, hidden_size=512):
       super(lorenzNet, self).__init__(recurrent, hidden_size, hidden_size)

       self.obs_shape = obs_shape

       # FIXME hacky, recover input shape from flattened observation space
       # assuming an 11x11 board and 3 non spatial features
       bs = 11
       self.other_shape = [3]
       input_channels = (obs_shape[0] - self.other_shape[0]) // (bs*bs)
       self.image_shape = [input_channels, bs, bs]
       print("AFSGSFGSG")
       print(self.image_shape)
       #assert np.prod(obs_shape) >= np.prod(self.image_shape)

       self.conv1 = nn.Sequential(
           nn.Conv2d(input_channels, 32, 3), # out = 9 x 9 x 32
           nn.ReLU(),
           nn.Conv2d(32, 64, 4), # out = 6 x 6 x 64
           nn.ReLU(),
           nn.Conv2d(64, 64, 3), # out = 4 x 4 x 64
           nn.ReLU(),
           nn.MaxPool2d(kernel_size=2,stride=1), # out 3 x 3 x 64
           nn.ReLU(),
           Flatten() #,
           #nn.Linear(64 * 4 * 4, hidden_size),
           #nn.ReLU()
       )

       self.linear = nn.Sequential(
           nn.Linear(64 * 3 * 3, hidden_size),
           nn.ReLU(),
           nn.Linear(hidden_size, hidden_size),
           nn.ReLU()
       )

       self.critic_linear = nn.Linear(hidden_size, 1)

   def forward(self, inputs, rnn_hxs, masks):

       inputs_image = inputs[:, :-self.other_shape[0]].view([-1] + self.image_shape)
       inputs_other = inputs[:, -self.other_shape[0]:]

       x = self.conv1(inputs_image)
       x = F.dropout(x)
       x = self.linear(x)

       # This is always false as of right now
       if self.is_recurrent:
           x, rnn_hxs = self._forward_gru(x, rnn_hxs, masks)

       return self.critic_linear(x), x, rnn_hxs
### ###


class MLPBase(NNBase):
    def __init__(self, num_inputs, recurrent=False, hidden_size=64):
        super(MLPBase, self).__init__(recurrent, num_inputs, hidden_size)

        if recurrent:
            num_inputs = hidden_size

        self.actor = nn.Sequential(
            nn.Linear(num_inputs, hidden_size),
            nn.Tanh(),
            nn.Linear(hidden_size, hidden_size),
            nn.Tanh()
        )

        self.critic = nn.Sequential(
            nn.Linear(num_inputs, hidden_size),
            nn.Tanh(),
            nn.Linear(hidden_size, hidden_size),
            nn.Tanh()
        )

        self.critic_linear = nn.Linear(hidden_size, 1)

    def forward(self, inputs, rnn_hxs, masks):
        x = inputs

        if self.is_recurrent:
            x, rnn_hxs = self._forward_gru(x, rnn_hxs, masks)

        hidden_critic = self.critic(x)
        hidden_actor = self.actor(x)

        return self.critic_linear(hidden_critic), hidden_actor, rnn_hxs
