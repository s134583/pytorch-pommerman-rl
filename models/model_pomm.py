import torch
import torch.nn as nn
import torch.nn.functional as F
from .model_generic import NNBase
import numpy as np


class ConvNet1(nn.Module):
    def __init__(
            self, input_shape, num_channels=1, output_size=64,
            batch_norm=True, activation_fn=F.relu, dilation=True):
        super(ConvNet1, self).__init__()

        assert len(input_shape) == 3
        assert input_shape[1] == input_shape[2]
        self.input_shape = input_shape
        self.num_channels = num_channels
        self.output_size = output_size
        self.batch_norm = batch_norm
        self.activation_fn = activation_fn
        self.flattened_size = num_channels * \
            (input_shape[1] - 2) * (input_shape[2] - 2)
        self.drop_prob = 0.2

        self.conv1 = nn.Conv2d(input_shape[0], num_channels, 5, stride=1)
        
        if self.batch_norm:
            self.bn1 = nn.BatchNorm2d(num_channels)
        else:
            self.bn1 = lambda x: x

        self.fc1 = nn.Linear(self.flattened_size, 70)
        self.fc2 = nn.Linear(70, output_size)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activation_fn(x)
        x = x.view(-1, self.flattened_size)
        x = self.fc1(x)
        x = self.activation_fn(x)
        out = self.fc2(x)

        return out


class ConvNet2(nn.Module):
    def __init__(
            self, input_shape, num_channels=2, output_size=512,
            batch_norm=True, activation_fn=F.relu, dilation=True):
        super(ConvNet2, self).__init__()

        assert len(input_shape) == 3
        assert input_shape[1] == input_shape[2]
        self.input_shape = input_shape
        self.num_channels = num_channels
        self.output_size = output_size
        self.batch_norm = batch_norm
        self.activation_fn = activation_fn
        self.flattened_size = num_channels * \
            (input_shape[1] - 2) * (input_shape[2] - 2)
        self.drop_prob = 0.2

        self.conv1 = nn.Conv2d(input_shape[0], num_channels, 5, stride=1, padding=2)
        self.conv2 = nn.Conv2d(num_channels, num_channels, 3, stride=1)

        if self.batch_norm:
            self.bn1 = nn.BatchNorm2d(num_channels)
            self.bn2 = nn.BatchNorm2d(num_channels)
        else:
            self.bn1 = lambda x: x
            self.bn2 = lambda x: x

        self.fc1 = nn.Linear(self.flattened_size, 100)
        self.fc2 = nn.Linear(100, output_size)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activation_fn(x)
        x = self.conv2(x)
        x = self.bn2(x)
        x = self.activation_fn(x)
        x = x.view(-1, self.flattened_size)
        x = self.fc1(x)
        x = self.activation_fn(x)
        out = self.fc2(x)

        return out


class ConvNet3(nn.Module):
    def __init__(
            self, input_shape, num_channels=64, output_size=512,
            batch_norm=True, activation_fn=F.relu, dilation=True):
        super(ConvNet3, self).__init__()

        assert len(input_shape) == 3
        assert input_shape[1] == input_shape[2]
        self.input_shape = input_shape
        self.num_channels = num_channels
        self.output_size = output_size
        self.batch_norm = batch_norm
        self.activation_fn = activation_fn
        self.flattened_size = num_channels * (input_shape[1] - 2) * (input_shape[2] - 2)
        self.drop_prob = 0.2

        self.conv1 = nn.Conv2d(input_shape[0], num_channels, 5, stride=1, padding=2)
        if dilation:
            self.conv2 = nn.Conv2d(num_channels, num_channels, 3, stride=1, dilation=2, padding=2)
        else:
            self.conv2 = nn.Conv2d(num_channels, num_channels, 3, stride=1, padding=1)
        self.conv3 = nn.Conv2d(num_channels, num_channels, 3, stride=1)

        if self.batch_norm:
            self.bn1 = nn.BatchNorm2d(num_channels)
            self.bn2 = nn.BatchNorm2d(num_channels)
            self.bn3 = nn.BatchNorm2d(num_channels)
        else:
            self.bn1 = lambda x: x
            self.bn2 = lambda x: x
            self.bn3 = lambda x: x

        self.fc1 = nn.Linear(self.flattened_size, 1024)
        self.fc2 = nn.Linear(1024, output_size)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activation_fn(x)
        x = self.conv2(x)
        x = self.bn2(x)
        x = self.activation_fn(x)
        x = self.conv3(x)
        x = self.bn3(x)
        x = self.activation_fn(x)
        x = x.view(-1, self.flattened_size)
        x = self.fc1(x)
        x = self.activation_fn(x)
        out = self.fc2(x)

        return out


class ConvNet4(nn.Module):
    def __init__(self, input_shape, num_channels=64, output_size=512,
                 batch_norm=True, activation_fn=F.relu, dilation=False):
        super(ConvNet4, self).__init__()

        assert len(input_shape) == 3
        assert input_shape[1] == input_shape[2]
        self.input_shape = input_shape
        self.num_channels = num_channels
        self.output_size = output_size
        self.batch_norm = batch_norm
        self.activation_fn = activation_fn
        self.flattened_size = num_channels * (input_shape[1] - 4) * (input_shape[2] - 4)
        self.drop_prob = 0.2

        self.conv1 = nn.Conv2d(input_shape[0], num_channels, 3, stride=1, padding=1)
        if dilation:
            self.conv2 = nn.Conv2d(num_channels, num_channels, 3, stride=1, dilation=2, padding=2)
        else:
            self.conv2 = nn.Conv2d(num_channels, num_channels, 3, stride=1, padding=1)
        self.conv3 = nn.Conv2d(num_channels, num_channels, 3, stride=1)
        self.conv4 = nn.Conv2d(num_channels, num_channels, 3, stride=1)

        if self.batch_norm:
            self.bn1 = nn.BatchNorm2d(num_channels)
            self.bn2 = nn.BatchNorm2d(num_channels)
            self.bn3 = nn.BatchNorm2d(num_channels)
            self.bn4 = nn.BatchNorm2d(num_channels)
        else:
            self.bn1 = lambda x: x
            self.bn2 = lambda x: x
            self.bn3 = lambda x: x
            self.bn4 = lambda x: x

        self.fc1 = nn.Linear(self.flattened_size, 1024)
        self.fc2 = nn.Linear(1024, output_size)

    def forward(self, x):
        x = self.conv1(x)
        #print("conv layer 1")
        #print(x.shape)
        x = self.bn1(x)
        x = self.activation_fn(x)
        x = self.conv2(x)
        #print("conv layer 2")
        #print(x.shape)
        x = self.bn2(x)
        x = self.activation_fn(x)
        x = self.conv3(x)
        #print("conv layer 3")
        #print(x.shape)
        x = self.bn3(x)
        x = self.activation_fn(x)
        x = self.conv4(x)
        #print("conv layer 4")
        #print(x.shape)
        x = self.bn4(x)
        x = self.activation_fn(x)
        x = x.view(-1, self.flattened_size)
        x = self.fc1(x)
        #print("dense layer")
        #print(x.shape)
        x = self.activation_fn(x)
        #x = F.dropout(x, p=self.drop_prob, training=self.training)
        out = self.fc2(x)
        #print("output layer")
        #print(out.shape)

        return out


class PommNet(NNBase):
    def __init__(self, obs_shape, recurrent=False, hidden_size=50, batch_norm=True, cnn_config=None):
        super(PommNet, self).__init__(recurrent, hidden_size, hidden_size)
        self.obs_shape = obs_shape
        self.cnn_config = cnn_config

        # FIXME hacky, recover input shape from flattened observation space
        # assuming an 11x11 board and 3 non spatial features
        bs = 11
        self.other_shape = [3]
        input_channels = (obs_shape[0] - self.other_shape[0]) // (bs*bs)
        self.image_shape = [input_channels, bs, bs]
        assert np.prod(obs_shape) >= np.prod(self.image_shape)
        
        if cnn_config == 'conv3':
            self.common_conv = ConvNet3(
                input_shape=self.image_shape,
                output_size=hidden_size,
                batch_norm=batch_norm)
        elif cnn_config == 'conv4':
            #assert cnn_config == 'conv4'
            self.common_conv = ConvNet4(
                input_shape=self.image_shape,
                output_size=hidden_size,
                batch_norm=batch_norm)
        elif cnn_config == 'conv2':
            self.common_conv = ConvNet2(
                input_shape=self.image_shape,
                output_size=hidden_size,
                batch_norm=batch_norm)
        elif cnn_config == 'conv1':
            self.common_conv = ConvNet1(
                input_shape=self.image_shape,
                output_size=hidden_size,
                batch_norm=batch_norm)
        else:
            pass # Do not include conv network here
        
        
        self.common_mlp = nn.Sequential(
            nn.Linear(self.other_shape[0], hidden_size//4),
            nn.ReLU(),
            nn.Linear(hidden_size//4, hidden_size//4),
            nn.ReLU()
        )

        '''
        # Tilfäj linært softmax til at få ned til 6 valg.
        self.actor = nn.Sequential(
            nn.Linear(hidden_size + hidden_size//4, hidden_size),
            nn.Tanh(),
            nn.Linear(in_features=hidden_size,
                                out_features=6,bias=False),
            nn.Softmax()
        )
        '''
        
        if cnn_config is not None:
            self.actor = nn.Linear(hidden_size + hidden_size//4, hidden_size)
        else:
            self.actor = nn.Linear( hidden_size//4, hidden_size)
        

        #self.critic = nn.Sequential(
        #    nn.Linear(hidden_size + hidden_size//4, 1),
        #    nn.Tanh()
        #)

    def forward(self, inputs):
        inputs_image = inputs[:, :-self.other_shape[0]].view([-1] + self.image_shape)
        inputs_other = inputs[:, -self.other_shape[0]:]
        
        if self.cnn_config is None:
            # Only linear
            x_mlp = self.common_mlp(inputs_other)
            x = x_mlp
        elif (self.cnn_config == 'conv1') or (self.cnn_config == 'conv3') or (self.cnn_config == 'conv4') or (self.cnn_config == 'conv2'):
            # Linear and conv
            x_mlp = self.common_mlp(inputs_other)
            x_conv = self.common_conv(inputs_image)
            x = torch.cat([x_mlp, x_conv], dim=1)
            #print('cat')
            #print(x.shape)
            #print(x_mlp.shape)
            #print(x_conv.shape)
        else:
            raise Exception('Specified network not implemented.')
        
        

        # HAR FJERNET RECURRENCE!!
        #if self.is_recurrent:
        #    x, rnn_hxs = self._forward_gru(x, rnn_hxs, masks)



        out_actor = self.actor(x)
        #out_value = self.critic(x)
        #print(out_actor.shape)

        return [], out_actor

    def es_params(self):
        """
        The params that should be trained by ES (all of them)
        """
        return [(k, v) for k, v in zip(self.state_dict().keys(),
                                       self.state_dict().values())]
