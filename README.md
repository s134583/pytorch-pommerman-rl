# Pommerman using Evolutionary Strategies and pytorch

We use the sep-CMA Evolutionary Strategy to train an agent to play Pommerman. This work is done as part of the final assignment for 02456 Deep Learning at The Technical University of Denmark, Kongens Lyngby, Denmark.

The work is based on the Pommerman competitions (https://www.pommerman.com/) and the brilliant work of Ross Wightman (https://github.com/rwightman/pytorch-pommerman-rl).

It requires the Pommerman `playground` (https://github.com/MultiAgentLearning/playground) to be installed in your Python environment. To setup this use the command

`python3 setup.py install`

Append `--user` to the above call if you do not have root access on your specific system.

## Usage

Run the below command to execute the trainer. Using the DTU cluster remember to `module load python3` beforehand.

`python3 main_ES.py --no-vis --env-name PommeFFACompetitionFast-v0 --no-norm --num-processes 4`

Below is a plot of the mean and median reward of a sample run. It ran for 500 generations with a starting sigma of 0.25. It plays againts two random and one simple agents. It does not perform as hoped, but it has learnt something.

![](results/Plot_run21.png)