import copy
import glob
import os
import time
import types
from collections import deque

import numpy as np
import torch

import algo
from arguments import get_args
from envs import make_vec_envs
from models import create_policy
from rollout_storage import RolloutStorage
from replay_storage import ReplayStorage
from visualize import visdom_plot

from models.model_pomm import PommNet

from distributions import Categorical, DiagGaussian

args = get_args()
args.num_steps = 10

assert args.algo in ['a2c', 'a2c-sil', 'ppo', 'ppo-sil', 'acktr']
if args.recurrent_policy:
    assert args.algo in ['a2c', 'ppo'], \
        'Recurrent policy is not implemented for ACKTR or SIL'

update_factor = args.num_steps * args.num_processes
num_updates = int(args.num_frames) // update_factor
lr_update_schedule = None if args.lr_schedule is None else args.lr_schedule // update_factor

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)
    print('hej CUDA')
np.random.seed(args.seed)

try:
    os.makedirs(args.log_dir)
except OSError:
    files = glob.glob(os.path.join(args.log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)

eval_log_dir = args.log_dir + "_eval"
try:
    os.makedirs(eval_log_dir)
except OSError:
    files = glob.glob(os.path.join(eval_log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)


def main():
    torch.set_num_threads(1)
    device = torch.device("cuda:0" if args.cuda else "cpu")
    print(device)

    if args.vis:
        from visdom import Visdom
        viz = Visdom(port=args.port)
        win = None

    train_envs = make_vec_envs(
        args.env_name, args.seed, args.num_processes, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)

    if args.eval_interval:
        eval_envs = make_vec_envs(
            args.env_name, args.seed + args.num_processes, args.num_processes, args.gamma,
            args.no_norm, args.num_stack, eval_log_dir, args.add_timestep, device,
            allow_early_resets=True, eval=True)

        if eval_envs.venv.__class__.__name__ == "VecNormalize":
            eval_envs.venv.ob_rms = train_envs.venv.ob_rms
    else:
        eval_envs = None

    actor_critic = create_policy(
        train_envs.observation_space,
        train_envs.action_space,
        #name='pomm',
        name='pomm',
        nn_kwargs={
            #'batch_norm': False if args.algo == 'acktr' else True,
            'recurrent': args.recurrent_policy,
            'hidden_size': 512,
        },
        train=True)

    actor_critic.to(device)

    rollouts = RolloutStorage(
        args.num_steps, args.num_processes,
        train_envs.observation_space.shape,
        train_envs.action_space,
        actor_critic.recurrent_hidden_state_size)

    obs = train_envs.reset()
    rollouts.obs[0].copy_(obs)
    rollouts.to(device)

    mean_reward = []
    episode_rewards = deque(maxlen=10)

    start = time.time()


    obs_space = train_envs.observation_space
    action_space = train_envs.action_space
    obs_shape = obs_space.shape

#net = PommNet(obs_shape=torch.tensor([9,11,11]))
#state_dict = net.state_dict()
#print(state_dict.items)

    rollouts.obs[0].copy_(obs)
    rollouts.to(device)

    mean_reward = []
    episode_rewards = deque(maxlen=10)

    start = time.time()

    obs = rollouts.obs[0]
    recurrent_hidden_states = rollouts.recurrent_hidden_states[0]
    print("Action space")
    print(action_space)


    net = PommNet(obs_shape=obs_shape)
    net.cuda()

    for param in net.parameters():
        param.requires_grad = False

    num_outputs = action_space.n
    print(num_outputs)

    print("PARAMETERS")
    groups = list(net.parameters())
    print(groups)
    print(len(groups))

    print("state dict")
    a = net.state_dict()
    #print(a)

    initial_model = PommNet(obs_shape=obs_shape)

    initial_model.load_state_dict(net.state_dict())

    # Make dictionary with means
    for (k, v)  in initial_model.es_params():
        print(v.size())
        #v_mean = np.mean(v)
        #print(v_mean)



    # ANTAGER KN EN ENKELT PROCESS
    rewards = np.empty((0,100), int)
    for j in range(10): # Set number of births
        for i in range(50):
            with torch.no_grad():
                value, action, action_log_prob = actor_critic.act2(obs)

            obs, reward, done, infos = train_envs.step(action)
            if done:
                print(i)
                print(reward)
                np.append(rewards , reward)
                break



'''
    for j in range(1):
        for step in range(args.num_steps):
            # Simple just play and update observation ectself.
            value, action, action_log_prob, recurrent_hidden_states = actor_critic.act(
                        obs,
                        recurrent_hidden_states,
                        rollouts.masks[0])
            print("det gør han")
            print(action)

            obs, reward, done, infos = train_envs.step(action)
            print(step)
            print("done?")
            print(done)
            #with torch.no_grad():
            #    value, action, action_log_prob, recurrent_hidden_states = actor_critic.act(
            #            rollouts.obs[step],
            #            rollouts.recurrent_hidden_states[step],
            #            rollouts.masks[step])

            # Obser reward and next obs
            obs, reward, done, infos = train_envs.step(action)

            # If done then clean the history of observations.
            masks = torch.tensor([[0.0] if done_ else [1.0] for done_ in done], device=device)
            rollouts.insert(obs, recurrent_hidden_states, action, action_log_prob, value, reward, masks)

            #for info in infos:
            #    if 'episode' in info.keys():
            #        episode_rewards.append(info['episode']['r'])
print('hello2')
'''
if __name__ == "__main__":
    main()
