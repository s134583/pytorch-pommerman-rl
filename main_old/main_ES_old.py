#coding=utf-8

#import copy
import glob
import os
import time
import types
from collections import deque
from multiprocess import Pool

import numpy as np
import torch
from scipy import linalg
from math import sqrt, exp, floor

import algo
from arguments import get_args
from envs import make_vec_envs
from models import create_policy
from rollout_storage import RolloutStorage
from replay_storage import ReplayStorage
from visualize import visdom_plot

from models.model_pomm import PommNet

from distributions import Categorical, DiagGaussian

from models.policy import Policy


args = get_args()
args.num_steps = 10

assert args.algo in ['a2c', 'a2c-sil', 'ppo', 'ppo-sil', 'acktr']
if args.recurrent_policy:
    assert args.algo in ['a2c', 'ppo'], \
        'Recurrent policy is not implemented for ACKTR or SIL'

update_factor = args.num_steps * args.num_processes
num_updates = int(args.num_frames) // update_factor
lr_update_schedule = None if args.lr_schedule is None else args.lr_schedule // update_factor

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)
np.random.seed(args.seed)

print(args.log_dir)

try:
    os.makedirs(args.log_dir)
except OSError:
    files = glob.glob(os.path.join(args.log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)

eval_log_dir = args.log_dir + "_eval"
try:
    os.makedirs(eval_log_dir)
except OSError:
    files = glob.glob(os.path.join(eval_log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)

def flatten_parameters(params): # From https://github.com/ctallec/world-models/blob/master/utils/misc.py
    """ Flattening parameters.
    :args params: generator of parameters (as returned by module.parameters())
    :returns: flattened parameters (i.e. one tensor of dimension 1 with all
        parameters concatenated)
    """
    #return torch.cat([p.detach().view(-1) for p in params], dim=0).cpu().numpy()
    return torch.cat([p.detach().view(-1) for p in params], dim=0)

def load_parameters(params, controller):
    """ Load flattened parameters into controller.
    :args params: parameters as a single 1D np array
    :args controller: module in which params is loaded
    """
    proto = next(controller.parameters())
    params = unflatten_parameters(
        params, controller.parameters(), proto.device)

    for p, p_0 in zip(controller.parameters(), params):
        p.data.copy_(p_0)

def unflatten_parameters(params, example, device):
    """ Unflatten parameters.
    :args params: parameters as a single 1D np array
    :args example: generator of parameters (as returned by module.parameters()),
        used to reshape params
    :args device: where to store unflattened parameters
    :returns: unflattened parameters
    """
    #params = torch.Tensor(params).to(device)
    idx = 0
    unflattened = []
    for e_p in example:
        unflattened += [params[idx:idx + e_p.numel()].view(e_p.size())]
        idx += e_p.numel()
    return unflattened



def main():
    #torch.set_num_threads(1)
    device = torch.device("cpu") # torch.device("cuda:0" if args.cuda else "cpu")
    print(device)

    if args.vis:
        from visdom import Visdom
        viz = Visdom(port=args.port)
        win = None

    # Consider calling something without _vec_..
    train_envs = make_vec_envs(
        args.env_name, args.seed, 1, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)

    obs_space = train_envs.observation_space
    action_space = train_envs.action_space
    obs_shape = obs_space.shape

    # Create network
    start_time = time.time()
    convtype = 'conv2' 
    init_net = PommNet(obs_shape=obs_shape, cnn_config=convtype)
    print(init_net)
    # Create a net to calculate number of parameters
    parameters = init_net.parameters()
    mean_updated = flatten_parameters(parameters)
    N = mean_updated.size()[0] # We take the size of the concattenated parameters
    
    # https://dylandjian.github.io/world-models/

    # Settings for the simulations
    pop_size = int(4 + floor(3*np.log(N))) #50 # Population size, i.e. how many chilred to draw in each generation
    N_games_pr_spring = 10
    max_length = 2000
    N_survive = int(pop_size/2)
    num_iter = 500

    # open txt file for results (write input arguments and initial setup)
    # REMEMBER TO UPDATE NAME. open a txt file of that name and append to it (use a+ if you want to create the file automatically).
    write_to_file = False
    if write_to_file:
        ff = open('runs/result_run13b.txt', 'a+')
        gg = open('runs/result_run13b_best.txt', 'a+')
        #log_interval = 10, lr = 0.00025, lr_schedule = None, max_grad_norm = 0.5, no_cuda = False, no_norm = True, num_frames = 50000000.0, num_mini_batch = 32, num_processes = 1, num_stack = 1, num_steps = 10
        ff.write("log_interval: {}, lr: {:,.5f}, lr_schedule: {}, max_grad_norm: {:,.1f}, no_cuda: {}, no_norm: {}, num_frames: {}, num_mini_batch: {}, num_processes: {}, num_stack: {}, num_steps: {} \n".format(
            args.log_interval, args.lr, args.lr_schedule, args.max_grad_norm, args.no_cuda, args.no_norm, args.num_frames, args.num_mini_batch, args.num_processes, args.num_stack, args.num_steps
        ))
        ff.write("Pop size: {}, Number of games per offspring: {}, Max length: {}, Number of survivors: {}, Number of iterations: {} \n \n".format(
            pop_size, N_games_pr_spring, max_length, N_survive, num_iter
        ))
        ff.write("iter, iteration time, elapsed time, mean reward, median reward, min reward, max reward, min fitness, avg fitness, max fitness, min sigma, max sigma \n")
    

    # Define function to play k games for a single offspring and save results
    def play_offspring(par_vec):#)  , N_games_pr_spring = N_games_pr_spring  , max_length = max_length , obs_0 = obs ):
        netti = PommNet(obs_shape=obs_shape, cnn_config=convtype)
        load_parameters(par_vec, netti)
        actor_evo = Policy(netti, action_space=action_space)
        train_envs = make_vec_envs( 
        args.env_name, args.seed, 1, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)  # Create single training environment instead of num process

        obs = train_envs.reset()
        all_rewards = np.empty(N_games_pr_spring, dtype='float32')

        fitness_tmp = max_length*np.ones(N_games_pr_spring, dtype=int)  # To initialize end game reward
        for k in range(N_games_pr_spring): # Number of games pr offspring
            for i in range(max_length): #max
                with torch.no_grad():
                    value, action, action_log_prob = actor_evo.act2(obs)
                
                obs, reward, done, infos = train_envs.step(action)
                
                #others = [3]
                #print(obs[:, -others[0]:])
                #bs = 11
                #try_input_channels = (obs_shape[0] -others[0]) // (bs*bs)
                #try_image_shape = [try_input_channels, bs, bs]
                #board = obs[:, :-others[0]].view([-1] +try_image_shape)
                #print(board)
                #print(board.shape)
                if done:
                    all_rewards[k] = reward # consider if a run does not reach done...
                    fitness_tmp[k] = (i + 100*reward) # How long did we survive
                    break

        return (np.mean(fitness_tmp), np.mean(all_rewards))


    # === CMA-ES ALGORITHM ===
    # Initialize
    # Fix w according to article
    w = [1/1.934212491*1/i**(1.3) for i in range(1, N_survive+1)] # weights
    mu_w = 1.0/sum(w)                       # mu_w
    p = Pool(args.num_processes)            # initialize parallization
    p_sigma = np.zeros(N, dtype='float32')  # evolution path for sigma, vector in R^N
    p_c = np.zeros(N, dtype='float32')      # evolution path for C, vector in R^N
    c_sigma = 3.0/N                         # backward time horizon for p_sigma (value from Wiki)
    C = np.identity(N, dtype='float32')     # covaraince matrix in R^(N x N)
    d_sigma = 0.9                           # damping factor (close to 1 form Wiki)
    sigma_k = 0.5                           # step-size (RANDOMLY CHOSEN - BETTER INIT VALUE MAY EXIST!)
    alpha = 0.5                             # constant scalar (value from Wiki)
    c_c = 4.0/(N+4)                         # backward time horizon for p_c (value form Wiki)
    c_1 = 2.0/N**2                          # learning rate for rank one update of covariance matrix
    c_mu = mu_w/N**2        

    print(N)
    for iter in range(num_iter): # MAYBE USE A STOPPING CRITERIA
        iter_start_time = time.time()

        # 1) Draw candidate solutions
        par = list()
        #print("draw from multi")
        deviations = C.diagonal()
        for _ in range(pop_size):
            #par.append(torch.from_numpy(
            #    np.random.multivariate_normal(mean_updated, C)).float())
            par.append(torch.from_numpy(np.random.normal(mean_updated,deviations,len(mean_updated))).float())
        #print("draw from multi done. Time")
        #print(time.time()-start_time)
        # 2) Play games to get fitness score of each candidate
        
        result = p.map(play_offspring , par) 
        #print("pool time")
        #print(time.time()-start_time)
        (fitness, all_rewards) = zip(*result)

        # 3) Extract survivors
        survivors = np.argsort(fitness)[::-1][:N_survive] # Index of the best N_survive offsprings 
        #survivors = np.argsort(fitness)[:N_survive]
        surv_list = [par[surv] for surv in survivors]
        surv_mat = torch.cat(surv_list, dim=0)
        surv_mat = surv_mat.view(N_survive, -1)
        
        # 4) Update mean
        mean_temp = mean_updated
        mean_updated = torch.empty(N)
        for i, weight in enumerate(w):
            mean_updated += surv_mat[i, :] * weight

        # 5) Update isotropic evolution path
        #C_inv_sqr = linalg.sqrtm(np.linalg.inv(C)) # Matrix square root of inverse of covariance matrix
        #print("C inv sqrt time")
        #print(time.time()-start_time)
        D = np.sqrt(1./C.diagonal())
        p_sigma = (1-c_sigma)*p_sigma + sqrt((1-(1-c_sigma)**2)) * sqrt(mu_w) * np.dot(D,(mean_updated-mean_temp)/sigma_k)
        #print("p_sigma size")
        #print(p_sigma.shape)

        # 6) Update step size
        exp_norm = sqrt(N)*(1.0-1.0/(4.0*N)+1.0/(21*N**2))
        p_sigma_norm = np.linalg.norm(p_sigma)
        sigma_k *= exp(c_sigma/d_sigma*(p_sigma_norm/exp_norm - 1))
        #print("sigma_k")
        #print(sigma_k)

        # 7) Update anisotropic evolution path
        if p_sigma_norm <= alpha*sqrt(N):
            p_c = (1-c_c)*p_c + sqrt(1-(1-c_c)**2)*sqrt(mu_w)*(mean_updated-mean_temp)/sigma_k
        else:
            p_c = (1-c_c)*p_c


        # 8) Update covariance matrix
        # first update c_s
        if p_sigma_norm**2 <= alpha*sqrt(N):
            c_s = (1-p_sigma_norm**2)*c_1*c_c*(2-c_c)
        else:
            c_s = c_1*c_c*(2-c_c)
        # then get last matrix in update
        summ = 0.0
        for iter_w, ww in enumerate(w):
            vec_temp = ((surv_mat[iter_w, :]-mean_updated)/sigma_k)
            summ += ww*np.dot(vec_temp,np.transpose(vec_temp))

        C = (1-c_1-c_mu+c_s)*C + c_1 * np.dot(p_c,np.transpose(p_c)) + c_mu * summ # actual update
        #print("Covariance")
        #print(C.shape)
        
        end_time = time.time()

        #if iter % 1 == 0:
        
        if write_to_file:
            gg.write("{:,.2f}, {:,.2f}, {:,.2f} \n".format(
                fitness[survivors[0]],
                all_rewards[survivors[0]],
                np.max(all_rewards))
            )
        
        print("iter: {}, iteration time: {:.1f}, elapsed time: {:.1f}".format(iter, (end_time - iter_start_time), (end_time - start_time)))
        print("mean/median reward {:.3f}/{:.1f}, min / max reward {:.1f}/{:.1f}, min / avg / max fitness {:.1f}/{:.1f}/{:.1f}".format(
            np.mean(all_rewards),
            np.median(all_rewards),
            np.min(all_rewards),
            np.max(all_rewards),
            np.min(fitness),
            np.average(fitness),
            np.max(fitness)
            ))
        #print(str(iter))
        #print(str(np.mean(all_rewards)))
        #print(str(np.max(all_rewards)))
        
        if write_to_file:
            ff.write("{}, {:.1f}, {:.1f}, {:.3f}, {:.1f}, {:.1f}, {:.1f}, {:.1f}, {:.1f}, {:.1f}\n".format(
                iter,
                (end_time - iter_start_time),
                (end_time - start_time),
                np.mean(all_rewards),
                np.median(all_rewards),
                np.min(all_rewards),
                np.max(all_rewards),
                np.min(fitness),
                np.average(fitness),
                np.max(fitness)
                ))
        
        #torch.min(sigma_surv),
        #torch.max(sigma_surv)
    
    if write_to_file:
        ff.close()
        gg.close()
        
    



'''
    for j in range(1):
        for step in range(args.num_steps):
            # Simple just play and update observation ectself.
            value, action, action_log_prob, recurrent_hidden_states = actor_critic.act(
                        obs,
                        recurrent_hidden_states,
                        rollouts.masks[0])
            print("det gør han")
            print(action)

            obs, reward, done, infos = train_envs.step(action)
            print(step)
            print("done?")
            print(done)
            #with torch.no_grad():
            #    value, action, action_log_prob, recurrent_hidden_states = actor_critic.act(
            #            rollouts.obs[step],
            #            rollouts.recurrent_hidden_states[step],
            #            rollouts.masks[step])

            # Obser reward and next obs
            obs, reward, done, infos = train_envs.step(action)

            # If done then clean the history of observations.
            masks = torch.tensor([[0.0] if done_ else [1.0] for done_ in done], device=device)
            rollouts.insert(obs, recurrent_hidden_states, action, action_log_prob, value, reward, masks)

            #for info in infos:
            #    if 'episode' in info.keys():
            #        episode_rewards.append(info['episode']['r'])
print('hello2')
'''
if __name__ == "__main__":
    main()
