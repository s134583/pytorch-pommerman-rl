import copy
import glob
import os
import time
import types
from collections import deque

import numpy as np
import torch

import algo
from arguments import get_args
from envs import make_vec_envs
from models import create_policy
from rollout_storage import RolloutStorage
from replay_storage import ReplayStorage
from visualize import visdom_plot

from models.model_pomm import PommNet

args = get_args()

assert args.algo in ['a2c', 'a2c-sil', 'ppo', 'ppo-sil', 'acktr']
if args.recurrent_policy:
    assert args.algo in ['a2c', 'ppo'], \
        'Recurrent policy is not implemented for ACKTR or SIL'

update_factor = args.num_steps * args.num_processes
num_updates = int(args.num_frames) // update_factor
lr_update_schedule = None if args.lr_schedule is None else args.lr_schedule // update_factor

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)
    print('hej CUDA')
np.random.seed(args.seed)

try:
    os.makedirs(args.log_dir)
except OSError:
    files = glob.glob(os.path.join(args.log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)

eval_log_dir = args.log_dir + "_eval"
try:
    os.makedirs(eval_log_dir)
except OSError:
    files = glob.glob(os.path.join(eval_log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)


def main():
    print("hello")
    torch.set_num_threads(1)
    device = torch.device("cuda:0" if args.cuda else "cpu")
    print(device)

    if args.vis:
        from visdom import Visdom
        viz = Visdom(port=args.port)
        win = None

    train_envs = make_vec_envs(
        args.env_name, args.seed, args.num_processes, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)

    if args.eval_interval:
        eval_envs = make_vec_envs(
            args.env_name, args.seed + args.num_processes, args.num_processes, args.gamma,
            args.no_norm, args.num_stack, eval_log_dir, args.add_timestep, device,
            allow_early_resets=True, eval=True)

        if eval_envs.venv.__class__.__name__ == "VecNormalize":
            eval_envs.venv.ob_rms = train_envs.venv.ob_rms
    else:
        eval_envs = None

    # FIXME this is very specific to Pommerman env right now
    actor_critic = create_policy(
        train_envs.observation_space,
        train_envs.action_space,
        #name='pomm',
        name='lorenz',
        nn_kwargs={
            #'batch_norm': False if args.algo == 'acktr' else True,
            'recurrent': args.recurrent_policy,
            'hidden_size': 512,
        },
        train=True)

    actor_critic.to(device)

    if args.algo.startswith('a2c'):
        agent = algo.A2C_ACKTR(
            actor_critic, args.value_loss_coef,
            args.entropy_coef,
            lr=args.lr, lr_schedule=lr_update_schedule,
            eps=args.eps, alpha=args.alpha,
            max_grad_norm=args.max_grad_norm)
    elif args.algo.startswith('ppo'):
        agent = algo.PPO(
            actor_critic, args.clip_param, args.ppo_epoch, args.num_mini_batch,
            args.value_loss_coef, args.entropy_coef,
            lr=args.lr, lr_schedule=lr_update_schedule,
            eps=args.eps,
            max_grad_norm=args.max_grad_norm)
    elif args.algo == 'acktr':
        agent = algo.A2C_ACKTR(
            actor_critic, args.value_loss_coef,
            args.entropy_coef,
            acktr=True)

    if args.algo.endswith('sil'):
        agent = algo.SIL(
            agent,
            update_ratio=args.sil_update_ratio,
            epochs=args.sil_epochs,
            batch_size=args.sil_batch_size,
            value_loss_coef=args.sil_value_loss_coef or args.value_loss_coef,
            entropy_coef=args.sil_entropy_coef or args.entropy_coef)
        replay = ReplayStorage(
            5e5,
            args.num_processes,
            args.gamma,
            0.1,
            train_envs.observation_space.shape,
            train_envs.action_space,
            actor_critic.recurrent_hidden_state_size,
            device=device)
    else:
        replay = None

    rollouts = RolloutStorage(
        args.num_steps, args.num_processes,
        train_envs.observation_space.shape,
        train_envs.action_space,
        actor_critic.recurrent_hidden_state_size)

    obs = train_envs.reset()
    rollouts.obs[0].copy_(obs)
    rollouts.to(device)

    mean_reward = []
    episode_rewards = deque(maxlen=10)

    start = time.time()
    net = PommNet()
    state_dict = net.state_dict()
    print(state_dict.items)
    print('hello2')

#for name, param in state_dict.items():
    # Transform the parameter as required.
    transformed_param = param * 0.9

    # Update the parameter.
    state_dict[name].copy_(transformed_param)
    for j in range(num_updates):
        for step in range(args.num_steps):
            # Sample actions
            print("hej")

if __name__ == "__main__":
    main()
