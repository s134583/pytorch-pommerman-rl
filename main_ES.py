#coding=utf-8

#import copy
import glob
import os
import time
import types
from collections import deque
from multiprocess import Pool

import numpy as np
import torch
from scipy import linalg
from math import sqrt, exp, floor, log

import algo
from arguments import get_args
from envs import make_vec_envs
from models import create_policy
from rollout_storage import RolloutStorage
from replay_storage import ReplayStorage
from visualize import visdom_plot

from models.model_pomm import PommNet

from distributions import Categorical, DiagGaussian

from models.policy import Policy

args = get_args()
args.num_steps = 10

assert args.algo in ['a2c', 'a2c-sil', 'ppo', 'ppo-sil', 'acktr']
if args.recurrent_policy:
    assert args.algo in ['a2c', 'ppo'], \
        'Recurrent policy is not implemented for ACKTR or SIL'

update_factor = args.num_steps * args.num_processes
num_updates = int(args.num_frames) // update_factor
lr_update_schedule = None if args.lr_schedule is None else args.lr_schedule // update_factor

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)
np.random.seed(args.seed)

try:
    os.makedirs(args.log_dir)
except OSError:
    files = glob.glob(os.path.join(args.log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)

eval_log_dir = args.log_dir + "_eval"
try:
    os.makedirs(eval_log_dir)
except OSError:
    files = glob.glob(os.path.join(eval_log_dir, '*.monitor.csv'))
    for f in files:
        os.remove(f)

def flatten_parameters(params): # From https://github.com/ctallec/world-models/blob/master/utils/misc.py
    """ Flattening parameters.
    :args params: generator of parameters (as returned by module.parameters())
    :returns: flattened parameters (i.e. one tensor of dimension 1 with all
        parameters concatenated)
    """
    #return torch.cat([p.detach().view(-1) for p in params], dim=0).cpu().numpy()
    return torch.cat([p.detach().view(-1) for p in params], dim=0)

def load_parameters(params, controller):
    """ Load flattened parameters into controller.
    :args params: parameters as a single 1D np array
    :args controller: module in which params is loaded
    """
    proto = next(controller.parameters())
    params = unflatten_parameters(
        params, controller.parameters(), proto.device)

    for p, p_0 in zip(controller.parameters(), params):
        p.data.copy_(p_0)

def unflatten_parameters(params, example, device):
    """ Unflatten parameters.
    :args params: parameters as a single 1D np array
    :args example: generator of parameters (as returned by module.parameters()),
        used to reshape params
    :args device: where to store unflattened parameters
    :returns: unflattened parameters
    """
    #params = torch.Tensor(params).to(device)
    idx = 0
    unflattened = []
    for e_p in example:
        unflattened += [params[idx:idx + e_p.numel()].view(e_p.size())]
        idx += e_p.numel()
    return unflattened


def main():
    #torch.set_num_threads(1)
    device = torch.device("cpu") # torch.device("cuda:0" if args.cuda else "cpu")
    print(device)

    if args.vis:
        from visdom import Visdom
        viz = Visdom(port=args.port)
        win = None

    # Consider calling something without _vec_..

    train_envs = make_vec_envs(
        args.env_name, args.seed, 1, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)

    obs_space = train_envs.observation_space
    action_space = train_envs.action_space
    obs_shape = obs_space.shape

    # Create network
    start_time = time.time()
    convtype = 'conv2'
    init_net = PommNet(obs_shape=obs_shape, cnn_config=convtype)

    # Create a net to calculate number of parameters
    parameters = init_net.parameters()
    x_mean = flatten_parameters(parameters)
    x_mean = x_mean.data.numpy() # Take out numpy
    N = x_mean.shape[0] # We take the size of the concattenated parameters
    
    # https://dylandjian.github.io/world-models/

    # Settings for the simulations
    N_games_pr_spring = 10
    max_length = int(5e4)
    num_iter = 500

    # open txt file for results (write input arguments and initial setup)
    # REMEMBER TO UPDATE NAME. open a txt file of that name and append to it (use a+ if you want to create the file automatically).
    print_to_file = True
    if print_to_file:
        ff = open('runs/result_run21.txt', 'a+')
        gg = open('runs/result_run21_best.txt', 'a+')

        ff.write("log_interval: {}, lr: {:,.5f}, lr_schedule: {}, max_grad_norm: {:,.1f}, no_cuda: {}, no_norm: {}, num_frames: {}, num_mini_batch: {}, num_processes: {}, num_stack: {}, num_steps: {} \n".format(
        args.log_interval, args.lr, args.lr_schedule, args.max_grad_norm, args.no_cuda, args.no_norm, args.num_frames, args.num_mini_batch, args.num_processes, args.num_stack, args.num_steps
        ))

    # Define function to play k games for a single offspring and save results
    def play_offspring(par_vec):#)  , N_games_pr_spring = N_games_pr_spring  , max_length = max_length , obs_0 = obs ):
        netti = PommNet(obs_shape=obs_shape, cnn_config=convtype)
        load_parameters(par_vec, netti)
        actor_evo = Policy(netti, action_space=action_space)
        train_envs = make_vec_envs( 
        args.env_name, args.seed, 1, args.gamma, args.no_norm, args.num_stack,
        args.log_dir, args.add_timestep, device, allow_early_resets=False)  # Create single training environment instead of num process

        obs = train_envs.reset()
        all_rewards = -1*np.ones(N_games_pr_spring, dtype='float32')

        fitness_tmp = -100*np.ones(N_games_pr_spring, dtype=int)  # To initialize end game reward
        for k in range(N_games_pr_spring): # Number of games pr offspring
            for i in range(max_length): #max
                with torch.no_grad():
                    value, action, action_log_prob = actor_evo.act2(obs)
                
                obs, reward, done, infos = train_envs.step(action)
                
                if done:
                    all_rewards[k] = reward # consider if a run does not reach done... (set max_length large to avoid )
                    fitness_tmp[k] = (i + 100*reward) # How long did we survive
                    
                    break

        return (np.mean(fitness_tmp), np.mean(all_rewards))

    p = Pool(args.num_processes)         # initialize parallel pool

    # === CMA-ES ALGORITHM ===
    pop_size = int(4 + floor(3*log(N))) # corresponds to lambda
    mu = int(floor(pop_size/2))         # number of off-springs
    sigma = 0.5                         # step size 

    # Write to file
    if print_to_file:
        ff.write("Pop size: {}, Number of games per offspring: {}, Max length: {}, Number of survivors: {}, Number of iterations: {}, Step size (sigma): {} \n \n".format(
            pop_size, N_games_pr_spring, max_length, mu, num_iter, sigma
        ))
        # REMEMBER TO FIX THESE FOR RUNS!!
        ff.write("Agents: TrainingAgent, RandomAgent, RandomAgent, RandomAgent\n\n")
        ff.write("iter, iteration time, elapsed time, mean reward, median reward, min reward, max reward, min fitness, avg fitness, max fitness, sigma \n")

    # Parameter setting (based in paper)
    #w = []  # Define weight vector
    w = np.zeros((mu,)) # Define weight vector
    for i in range(1,mu+1):
        den = 0.0
        for j in range(1,mu+1):
            den += (log(mu+1.0) - log(j))
        w[i-1] = ((log(mu+1.0) - log(i))/den)
    
    mu_w = 1.0/np.sum(np.square(w)) #mu_w = 1.0/sum(w) 
    c_sigma = (mu_w+2.0)/(N+mu_w+3.0) # backward time horizon for p_sigma
    d_temp = (mu_w-1.0)/(N+1.0)-1 # Need this update for d because we get error when sqrt(negative number) occurs.
    if d_temp <= 0:
        d_sigma = 1 + c_sigma  # damping factor
    else:
        d_sigma = 1 + 2.0*sqrt(d_temp) + c_sigma
    c_c = 4.0/(N+4)   # backward time horizon for p_c
    mu_cov = mu_w
    c_cov_def = 1.0/mu_w * 2.0/(N+sqrt(2.0)**2) + (1.0 - 1.0/mu_cov) * min(1.0, (2.0*mu_cov-1.0)/((N+2.0)**2+mu_cov))
    c_cov = (N+2.0)/3.0 * c_cov_def
    exp_norm = sqrt(N)*(1.0-1.0/(4.0*N)+1.0/(21*N**2))  # = E(||N(0,I)||)

    # Initialization (based on paper)
    p_sigma = np.zeros(N, dtype='float32')  # evolution path for sigma, vector in R^N
    p_c = np.zeros(N, dtype='float32')      # evolution path for C, vector in R^N       
    C = np.identity(N, dtype='float32')     # covaraince matrix in R^(N x N)
    D = np.ones(N, dtype='float32')         # In CMA-ES: eig(C) = B D^{-1} B^T. For sep-CMA-ES: B = I, D is diag

    print("Number of paramaters: " + str(N))

    for iter in range(num_iter): # MAYBE USE A STOPPING CRITERIA
        iter_start_time = time.time()

        # 1) Draw candidate solutions
        # Assume diagonal covariance => draw from 1D normal distribution. Greatly reduces computation time.
        z = [] 
        x = []
        for _ in range(pop_size):
            z_temp = np.random.normal(np.zeros(N), np.ones(N), N)
            x.append(x_mean + sigma * D * z_temp)
            z.append(z_temp)

        # 2) Play games to get fitness score of each candidate
        x_torch = [torch.from_numpy(x_) for x_ in x]
        result = p.map(play_offspring, x_torch) 
        (fitness, all_rewards) = zip(*result)

        # 3) Extract survivors
        survivors = np.argsort(fitness)[::-1][:mu] # Index of the best mu offsprings 
        x_surv = np.empty((N,mu))
        z_surv = np.empty((N,mu))
        for ids, surv in enumerate(survivors):
            x_surv[:,ids] = x[surv]
            z_surv[:,ids] = z[surv]

        # 4) Update mean
        x_mean = np.zeros((N,))
        z_mean = np.zeros((N,))
        for i, weight in enumerate(w):
            x_mean += weight * x_surv[:,i]
            z_mean += weight * z_surv[:,i]

        # 5) Update isotropic evolution path
        p_sigma = (1-c_sigma)*p_sigma + sqrt(c_sigma*(2.0-c_sigma))*sqrt(mu_w) * z_mean

        # 6) Update anisotropic evolution path
        p_sigma_norm = np.linalg.norm(p_sigma)
        if p_sigma_norm/(sqrt(1.0-(1.0-c_sigma)**(2*(iter+1)))) < (1.4+2.0/(N+1.0))*exp_norm:
            p_c = (1-c_c)*p_c + sqrt(c_sigma*(2.0-c_sigma))*sqrt(mu_w)* (D*z_mean)
        else:
            p_c = (1-c_c)*p_c

        # 7) Update covariance matrix
        summ = np.empty((N,N))
        for iter_w, ww in enumerate(w):
            vec_temp = D*z_surv[:,iter_w]
            summ += ww*np.outer(vec_temp, vec_temp)

        C = (1.0-c_cov)*C + c_cov/mu_cov * np.outer(p_c,p_c) + c_cov*(1.0-1.0/mu_cov) * summ  # actual update

        # 8) Update step size
        sigma *= exp(c_sigma/d_sigma*(p_sigma_norm/exp_norm - 1))
        
        # 9) Update D (normally eigendecomposition)
        D = np.sqrt(C.diagonal())

        end_time = time.time()
        if print_to_file:
            gg.write("{:,.2f}, {:,.2f}, {:,.2f} \n".format(
                fitness[survivors[0]],
                all_rewards[survivors[0]],
                np.max(all_rewards))
            )
        
        print("iter: {}, iteration time: {:.1f}, elapsed time: {:.1f}".format(iter, (end_time - iter_start_time), (end_time - start_time)))
        print("mean/median reward {:.3f}/{:.1f}, min / max reward {:.1f}/{:.1f}, min / avg / max fitness {:.1f}/{:.1f}/{:.1f}".format(
            np.mean(all_rewards),
            np.median(all_rewards),
            np.min(all_rewards),
            np.max(all_rewards),
            np.min(fitness),
            np.average(fitness),
            np.max(fitness)
            ))
        
        if print_to_file:
            ff.write("{}, {:.1f}, {:.1f}, {:.3f}, {:.1f}, {:.1f}, {:.1f}, {:.1f}, {:.1f}, {:.1f}, {:.1f}\n".format(
                iter,
                (end_time - iter_start_time),
                (end_time - start_time),
                np.mean(all_rewards),
                np.median(all_rewards),
                np.min(all_rewards),
                np.max(all_rewards),
                np.min(fitness),
                np.average(fitness),
                np.max(fitness),
                sigma
                ))


        np.save('runs/result_run16_param_final', x_surv[:,0])

    if print_to_file:    
        ff.close()  
        gg.close()
    

if __name__ == "__main__":
    main()
