#!/bin/sh
### General options
### -- specify queue and jobname --
#BSUB -q compute
#BSUB -J pommerman_evolution

### -- ask for number of cores (default: 1) --
#BSUB -n 24
#BSUB -R "span[hosts=1]"
#BSUB -R "select[model=XeonE5_2650v4]"

# Memory specifications. Amount we need and when to kill the
# program using too much memory.
#BSUB -R "rusage[mem=10GB]"
#BSUB -M 10GB

### -- set walltime limit: hh:mm --
#BSUB -W 20:00

### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u YOUR_EMAIL
### -- send notification at start --
#BSUB -B
### -- send notification at completion --
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -oo tmp_logs/%J.out
#BSUB -eo tmp_logs/%J.err

# Load modules
module load python3/3.6.2

# Activate virtual environment. Try current folder otherwise on folder above
if [ -e virtenv/bin/activate ]
then
    echo "virtenv found in virtenv/bin/activate"
    source virtenv/bin/activate
elif [ -e ../virtenv/bin/activate ]
then
    echo "virtenv found in ../virtenv/bin/activate"
    source ../virtenv/bin/activate
else
    echo "Virtual env not found... Exiting"
    exit -1
fi

# Start script
echo "Starting python script"
python3 main_ES.py --no-vis --env-name PommeFFACompetitionFast-v0 --no-norm --num-processes $LSB_DJOB_NUMPROC