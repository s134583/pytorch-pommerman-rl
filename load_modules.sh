#!/bin/sh

module load python3/3.6.2
module load cuda/9.2
module load openblas/0.2.20
module load numpy/1.13.1-python-3.6.2-openblas-0.2.20